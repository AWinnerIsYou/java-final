public class PentHouse extends Room{

    public PentHouse(int floor, int hallNumber){
	super(floor, hallNumber);
	RoomDetails();
    }

    public void RoomDetails(){
	this.setPrice(500);
	this.setNumBeds(4);
	this.setOccupied(false);
	this.setStatus("available");
    }

    public String toString(){
	return(this.getFloor() + "-"  + this.getHallNumber());
    }


}
