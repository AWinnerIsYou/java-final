interface RoomActions {
    void setStatus(String status);
    void setCheckedIn(boolean a);
    void setOccupied(boolean a);
    void setOccupant(String name);
    void setPrice(int price);
    void setNumBeds(int beds);
    void setFloor(int floor);
}
