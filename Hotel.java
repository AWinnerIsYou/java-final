public class Hotel{

    private Room[][]hotel;

    public Hotel(){
	hotel = new Room[5][5];
	for (int i = 0; i < hotel.length; i++){
	    for (int j = 0; j < hotel[i].length; j++){
		int f = hotel.length-i;
		int r = j+1;
		if(f == 5 && r == 3)
		    hotel[i][j] = new PentHouse(f,r);
		else{
		    if(f > 3)
			hotel[i][j] = new Luxury(f,r);
		    else
			hotel[i][j] = new Standard(f,r);
		}
	    }
	}
    }

    public void printUnoccupied(){
	for (int i = 0; i < hotel.length; i++){
	    for (int j = 0; j < hotel[i].length; j++){
		if (!(hotel[i][j].getOccupied()))
		    System.out.print(hotel[i][j] + " ");
		else
		    System.out.print("    ");
	    }
	    System.out.println();
	}
    }

    public boolean isAnyReserved(){
	for (int i = 0; i < hotel.length; i++){
	    for(int j = 0; j < hotel[i].length; j++){
		if (hotel[i][j].getOccupied() && (hotel[i][j].getStatus()).equals("reserved")){
		    return true;
		}
	    }
	}
	return false;
    }

    public boolean isRoomAvailable(){
	for (int i = 0; i < hotel.length; i++){
	    for(int j = 0; j < hotel[i].length; j++){
		if (!(hotel[i][j].getOccupied())){
		    return true;
		}
	    }
	}
	return false;
    }


    public boolean isCheckedIn(){
	for (int i = 0; i < hotel.length; i++){
	    for(int j = 0; j < hotel[i].length; j++){
		if (hotel[i][j].getCheckedIn()){
		    return true;
		}
	    }
	}
	return false;
    }



    public Room getRoom(int floor, int hallNumber){
	return (hotel[hotel.length-floor][hallNumber-1]);
    }
}
