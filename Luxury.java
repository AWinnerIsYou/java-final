public class Luxury extends Room{

    public Luxury(int floor, int hallNumber){
	super(floor, hallNumber);
	RoomDetails();
    }

    public void RoomDetails(){
	this.setPrice(300);
	this.setNumBeds(3);
	this.setOccupied(false);
	this.setStatus("available");
    }

    public String toString(){
	return(this.getFloor() + "-"  + this.getHallNumber());
    }

}

