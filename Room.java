public abstract class Room implements RoomActions{

    private int _floor, _hallNumber, _price, _numBeds;
    private String _occupant, _status;
    private boolean _isOccupied, _checkedIn;

    public Room(int floor, int hallNumber){
	_floor = floor;
	_hallNumber = hallNumber;
    }

    public String getStatus(){
	return _status;
    }

    public void setStatus(String s){
	_status = s;
    }

    public boolean getCheckedIn(){
	return _checkedIn;
    }

    public void setCheckedIn(boolean a){
	_checkedIn = a;
    }

    public boolean getOccupied(){
	return _isOccupied;
    }

    public void setOccupied(boolean a){
	_isOccupied = a;
    }

    public String getOccupant(){
	return _occupant;
    }

    public void setOccupant(String name){
	_occupant = name;
    }

    public int getPrice(){
	return _price;
    }

    public int getNumBeds(){
	return _numBeds;
    }

    public void setPrice(int n){
	_price = n;
    }

    public void setNumBeds(int n){
	_numBeds = n;
    }

    public int getFloor(){
	return _floor;
    }

    public int getHallNumber(){
	return _hallNumber;
    }

    public void  setFloor(int n){
	_floor = n;
    }

    public void setHallNumber(int n){
	_hallNumber = n;
    }

    public abstract void RoomDetails();

}
