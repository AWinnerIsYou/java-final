import java.util.Scanner;

public class HotelApp{

    public static void main(String[]args){
	//taken from https://stackoverflow.com/questions/4062045/clearing-terminal-in-linux-with-c-code
	System.out.print("\033[2J\033[1;1H");
	Hotel inn = new Hotel();
	System.out.println("Welcome to Our Hotel!!");
	Scanner s = new Scanner(System.in);
	System.out.println();
	while (true){
	    System.out.print("Please Type if you would like to make a: Reservation, Check In, Check Out, or Exit: ");
	    String request = s.nextLine();
	    if (request.equalsIgnoreCase("Reservation")){
		MakeAReservation(inn);
	    }
	    if(request.equalsIgnoreCase("Check In")){
		CheckIn(inn);
	    }
	    if(request.equalsIgnoreCase("Check Out")){
		CheckOut(inn);
	    }
	    if(request.equalsIgnoreCase("Exit")) {
		System.exit(0);
	    }
	}
    }
    
    //taken from: https://stackoverflow.com/questions/237159/whats-the-best-way-to-check-to-see-if-a-string-represents-an-integer-in-java
    public static boolean isInteger(String str) {
	if (str == null)
	    return false;
	int length = str.length();
	if (length == 0)
	    return false;
	int i = 0;
	if (str.charAt(0) == '-'){
	    if (length == 1)
		return false;
	    i = 1;
	}
	for (; i < length; i++){
	    char c = str.charAt(i);
	    if (c < '0' || c > '5')
		return false;
	}
	return true;
    }
    
    public static boolean RoomNumberValidation(String roomNumber){
	if (roomNumber.length() != 3)
	    return true;
	if (!(roomNumber.substring(1,2).equals("-")))
	    return true;
	if (!(isInteger(roomNumber.substring(0,1))))
	    return true;
	if (!(isInteger(roomNumber.substring(2,3))))
	    return true;
	else
	    return false;
    }

    public static void MakeAReservation(Hotel inn){
	if (!(inn.isRoomAvailable())){
	    System.out.println("Sorry all our rooms are reserved, or checked in. Please come back later when there is an opening.");
	}
	else{
	    System.out.println("Welcome to the Reservation Department");
	    System.out.println();
	    Scanner s = new Scanner(System.in);
	    System.out.println("Here is a list of available rooms");
	    inn.printUnoccupied();
	    System.out.println("All Rooms From the Third Floor and Below are STANDARD");
	    System.out.println("All Rooms On the Fifth and Fourth Floors are LUXURY");
	    System.out.println("The Room 5-3 is the PENT HOUSE");
	    System.out.println("STANDARD: Cost is $200 per night and contains 2 beds");
	    System.out.println("LUXURY: Cost is $300 per night and contains 3 beds");
	    System.out.println("PENT HOUSE: Cost is $500 per night and contains 4 beds");
	    System.out.println();
	    System.out.print("Please Select Which Room you would like to Stay in: ");
	    String roomNumber = s.nextLine();
	    if (RoomNumberValidation(roomNumber))
		System.out.println("That is not a valid room.");
	    else{
		int floor = Integer.parseInt(roomNumber.substring(0,1));
		int hallNumber = Integer.parseInt(roomNumber.substring(2,3));
		if (floor > 5 || floor < 1 || hallNumber > 5 || hallNumber < 1)
		    System.out.println("That is not a valid room.");
		Room room = inn.getRoom(floor,hallNumber);
		if (room.getOccupied()){
		    System.out.println("Sorry, that room is not available.");
		    MakeAReservation(inn);
		}
		else{
		    System.out.println("Please Enter your First and Last Name");
		    System.out.print("First Name: ");
		    String fName = s.nextLine();
		    System.out.print("Last Name: ");
		    String lName = s.nextLine();
		    room.setOccupant(fName + " " + lName);
		    room.setOccupied(true);
		    room.setStatus("reserved");
		    System.out.println("Congratulations! You Are officially Reserved for Room " + roomNumber + "!");
		}
	    }
	}
    }

    public static void CheckIn(Hotel inn){
	if(!(inn.isAnyReserved())){
	    System.out.println("Sorry all our rooms are checked in, or haven't been reserved. Please make a reservation, or wait for an opening.");
	}
	else{
	    System.out.println("Welcome to the Check In Department");
	    System.out.println();
	    Scanner s = new Scanner(System.in);
	    System.out.print("Please Enter your Room Number: ");
	    String rN = s.nextLine();
	    if (RoomNumberValidation(rN))
		System.out.println("That is not a valid room.");
	    else{
		int floor = Integer.parseInt(rN.substring(0,1));
		int hallNumber = Integer.parseInt(rN.substring(2,3));
		Room room = inn.getRoom(floor,hallNumber);
		if (!(room.getOccupied()))
		    System.out.println("Sorry, no Reservation was made in this room");
		else{
		    if(room.getCheckedIn())
			System.out.println("Sorry, this room has already been checked in");
		    else{
			System.out.println("Please Enter your First and Last Name");
			System.out.print("First Name: ");
			String fName = s.nextLine();
			System.out.print("Last Name: ");
			String lName = s.nextLine();
			if ((room.getOccupant()).equals(fName + " " + lName)){
			    room.setCheckedIn(true);
			    room.setStatus("checkedIn");
			    System.out.println("You have successfully Checked In");
			}
			else{
			    System.out.println("Sorry that information does not match our records, please try again.");
			    CheckIn(inn);
			}
		    }
		}
	    }
	}
    }

    public static void CheckOut(Hotel inn){
	if(!(inn.isCheckedIn())){
	    System.out.println("Sorry there are no rooms that are checked in. Please make a reservation, or wait for an opening.");
	}
	else{
	    System.out.println("Welcome to the Check Out Department");
	    System.out.println();
	    Scanner s = new Scanner(System.in);
	    System.out.print("Please Enter your Room Number: ");
	    String rN = s.nextLine();
	    if (RoomNumberValidation(rN))
		System.out.println("That is not a valid room.");
	    else{
		int floor = Integer.parseInt(rN.substring(0,1));
		int hallNumber = Integer.parseInt(rN.substring(2,3));
		Room room = inn.getRoom(floor,hallNumber);
		if (!(room.getOccupied()))
		    System.out.println("Sorry, no Reservation was made in this room");
		else{
		    if(!(room.getCheckedIn()))
			System.out.println("Sorry, this room has not been checked in");
		    else{
			System.out.println("Please Enter your First and Last Name");
			System.out.print("First Name: ");
			String fName = s.nextLine();
			System.out.print("Last Name: ");
			String lName = s.nextLine();
			if ((room.getOccupant()).equals(fName + " " + lName)){
			    room.setCheckedIn(false);
			    room.setOccupant(null);
			    room.setOccupied(false);
			    room.setStatus("available");
			    System.out.println("You have successfully Checked Out");
			}
			else{
			    System.out.println("Sorry that information does not match our records, please try again.");
			    CheckOut(inn);
			}
		    }
		}
	    }
	}

    }

}



































