public class Standard extends Room{

    public Standard(int floor, int hallNumber){
	super(floor, hallNumber);
	RoomDetails();
    }

    public void RoomDetails(){
	this.setPrice(200);
	this.setNumBeds(2);
	this.setOccupied(false);
	this.setStatus("available");
    }

    public String toString(){
	return(this.getFloor() + "-"  + this.getHallNumber());
    }
}
